import { defineStore } from "pinia";
import items from "../data.base/table.json";
import { api } from 'boot/axios';

export const mainStore = defineStore("main", {
  state: () => ({
    loading: false,
    rows: [],
  }),
  getters:{
    getRows(state){
      return state.rows
    }
  },
  actions: {
    async apiAuthorization(){
      await this.setLoading();
      let success = false;
      let token = null;
      try{
        await api.get('/key.html')
          .then(res => res.data)
          .then( res => {
          token = findToken(res);
          success = true;
        })
      }catch (err){
        console.error(err.message);
      }
      return { success, token }
    },

    async setLoading() {
      this.loading = true;
      const res = new Promise(resolve => {
        setTimeout(() => {
          this.loading = false;
          resolve(true)
        }, 1000)
      });
      return res;
    },

    async apiGetItems(){
      await this.setLoading();
      if(!items) return;
      this.rows = items
    }
  },
});


const findToken = (html) => {

  const parser = new DOMParser();
  const htmlDoc = parser.parseFromString(html, "text/html");
  const metaToken = htmlDoc.getElementsByTagName("META").token;

  if(metaToken === undefined) return null;
  if(metaToken.content === '') return null;
  return metaToken.content;

}
