const routes = [
  {
    path: "/",
    name: 'Main',
    meta: { layout: 'main' },
    component: () => import("pages/IndexPage.vue")
  },
  {
    path: "/login",
    name: 'Login',
    meta: { layout: 'auth' },
    component: () => import("pages/LoginPage.vue")
  },

  {
    path: "/:catchAll(.*)*",
    meta: { layout: 'main' },
    component: () => import("pages/ErrorNotFound.vue"),
  },

];

export default routes;
